# Create an empty config dict
config = dict()

##
#
#  Main server configuration
#
##

# Domains are handled automatically, only put listening address here
config["HOST"] = "127.0.0.1"
config["PORT"] = 8282

# name of the default theme directory (just the directory name, no additional /'s or anything.)
## Okay but seriously, I only added this in so I can use a git submodule with a custom theme. if you want to change the default theme, copy it and put the new folder name here
## and again, seriously, leave this as theme unless you've copied and changed it.
config["THEME_FOLDER"] = "quadfile-theme"

# Will output more logging data from QuadFile's logger
config["DEBUG"] = False

# Extended Debug will enable flask debugging. Keep this off for production use
config["EXTENDED_DEBUG"] = False

# Single user authentication, leave blank to disable authentication
config["KEY"] = ""

# Generates a deletion link for files
config["GEN_DELETEKEY"] = True

# File settings
config["UPLOAD_FOLDER"] = './data'
config["ALLOW_ALL_FILES"] = False
config["ALLOWED_MIMETYPES"] = set(['plain/text', 'application/pdf', 'image/png', 'image/x-png', 'image/jpeg', 'image/gif'])

# Will use blacklist if this is enabled. You must disable ALLOW_ALL_FILES for this to take effect
config["BLACKLIST"] = True
config["BANNED_MIMETYPES"] = set(['application/x-dosexec', 'application/x-msdownload', 'application/x-msdos-program' 'text/html', 'application/javascript', 'application/x-httpd-php', 'application/msi'])

# If this is set to true, old files will be deleted. TIME is how far behind (in seconds) the last accessed time can be before files get deleted
config["DELETE_FILES"] = True
config["TIME"] = 604800
config["CLEAN_INTERVAL"] = 300

# Site info displayed to the user
config["SITE_DATA"] = {
  "title": "QuadFile",
  "size": "100 MiB" # This is only for display, please limit filesize using your web server with nginx "client_max_body_size"
}
