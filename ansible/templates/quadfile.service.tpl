[Unit]
Description=QuadFile + Gunicorn Server
After=network.target

[Service]
PermissionsStartOnly = true
User = {{ quadfile_user }}
Group = {{ quadfile_user }}
WorkingDirectory = {{ app_location }}

ExecStart = /usr/local/bin/gunicorn wsgi:app -w 8 --bind 127.0.0.1:8282 --log-file /var/log/quadfile/quadfile.log
ExecReload = /bin/kill -s HUP $MAINPID
ExecStop = /bin/kill -s TERM $MAINPID

[Install]
WantedBy = multi-user.target